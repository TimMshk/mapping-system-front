import {sortCols, getFilterCategories, toKebabCase, toFiler} from "./functions";

// Изменение списка таблиц
const updateTablesList = (state, data) => {
  state.subTablesList = [];
  state.masterTables = [];
  data.forEach((elem) => {
    if (elem.attributes.is_master) {
      elem.subTables = [];
      state.masterTables.push(elem);
    }
  });
  data.forEach((elem) => {
    state.masterTables.forEach((masterTable) => {
      if (!elem.attributes.is_master && masterTable.attributes.category === elem.attributes.category) {
        masterTable.subTables.push(elem)
      }
    })
  });
}

// апдейт мастера
const updateData = (state, data) => {
  let mainTableData = []
  let treeData = []

  if (!data.flag) {
    state.treeData = [];
  }
  state.mainTableData = [];
  if (data.data) {
    data.data.forEach((elem, idx) => {
      let name = elem.attributes['product-name-eng'];
      if (elem.attributes['product-name-rus']) {
        name = elem.attributes['product-name-rus']
      }

      let o = Object.entries(elem.attributes).reduce((ooo, el) => {
        ooo[el[0].replace(/-/g, '').toLowerCase()] = el[1];
        return ooo
      }, {});

      const item = {
        id: elem.id,
        text: name,
        type: elem.attributes.child_count != '0' ? 'folder' : 'file',
        ...o,
        isMaster: true
      };

      mainTableData.push(item);
      if (elem.attributes.child_count !== '0' && !data.flag) {
        treeData.push(item)
      }
    })
  }

  state.mainTableData = mainTableData
  state.treeData = treeData
  // Посчитаем число страниц для вывода
  state.page.master.pagesCount = Math.ceil(data.totalRecords / state.page.size)
}

//апдейт sub
const updateSubData = (state, data) => {
  state.subTableData = [];
  let subTableData = []
  if (data.data) {
    data.data.forEach((elem) => {
      let o = Object.entries(elem.attributes).reduce((ooo, el) => {
        ooo[el[0].replace(/-/g, '').toLowerCase()] = el[1];
        return ooo
      }, {});
      const item = {
        id: elem.id,
        ...o,
      };
      subTableData.push(item);
    })
  }
  state.subTableData = subTableData;
  // Посчитаем число страниц для вывода
  state.page.sub.pagesCount = Math.ceil(data.totalRecords / state.page.size)
}

// апдейт списка колонок
const updateTablesCols = (state, data) => {
  state.subTableCols = [];
  state.activeSubFields = [];

  data.forEach((elem) => {
    if (elem.attributes.field === 'ID') {
      return false
    }

    let name = elem.attributes.field;
    if (elem.attributes['synonym-rus']) {
      name = elem.attributes['synonym-rus']
    }
    let dataName = elem.attributes.field;

    // список фильтров по категориям
    let obj = {
      name: name,
      dataName: dataName,
      filtered: true,
      isActive: elem.attributes['displayed-by-default'],
      filerField: elem.attributes.field === 'Mapping ID' ? 'Mapping Text' : elem.attributes.field,
      filterCondition: null,
      filterValue: '',
      apiFieldName: elem.attributes['field-back']
    };
    if (elem.attributes['table-path'] === state.activeSubTable.attributes['table_path']) {
      obj.filterCategories = getFilterCategories(state, dataName, 'subTableData');
      state.subTableCols.push(obj);
      if (elem.attributes['displayed-by-default']) {
        state.activeSubFields.push(toKebabCase(dataName))
      }
    } else if (elem.attributes['table-path'] === state.activeMainTable['table_path'] && !state.onlySub) {
      obj.filterCategories = getFilterCategories(state, dataName, 'mainTableData');
      state.mainTableCols.push(obj);
      if (elem.attributes['displayed-by-default']) {
        state.activeMainFields.push(toKebabCase(dataName))
      }
    }
  });
  state.onlySub = false;
  // ТУТ ДОЖДАТЬСЯ ПРОМИСА НАДО
  setTimeout(() => {
    sortCols(state.mainTableData, state.mainTableCols)
    sortCols(state.subTableData, state.subTableCols)
  }, 1000)
}

// Сортировка массива по возрастанию/убыванию
const sortArray = (state, direction) => {
  let FT = 'mainTableData';
  if (state.filteredTable === 2) {
    FT = 'subTableData'
  }

  let field = state.activeFilter;

  state[FT].sort((a, b) => {
    let p1 = a[field];
    let p2 = b[field];

    // Учитываем null и undefined значения
    if (!p1) return 1
    if (!p2) return -1

    p1 = !Object.is(+p1, NaN) ? +p1 : p1.toLowerCase();
    p2 = !Object.is(+p2, NaN) ? +p2 : p2.toLowerCase();

    if (p1 > p2) {
      return 1;
    }
    if (p1 < p2) {
      return -1;
    }
    return 0;
  });
  if (direction === 'bottom') {
    state[FT].reverse()
  }
}

// изменение списка брендов
const updBrandList = (state, props) => {
  props.data.forEach((elem) => {
    if (!state.brandList.find((brand) => {
      return brand.text === elem.attributes['brand-name-eng']
    })) {
      let brand = {
        text: elem.attributes['brand-name-eng'],
        icon: 'mdi-folder-multiple'
      };
      state.brandList.push(brand)
    }
  });
}

// изменение списка условия для master и sub таблиц
const updFilterConditions = (state, props) => {
  let table = 'mainFilterConditions';
  if (state.filteredTable === 2) {
    table = 'subFilterConditions'
  }
  state[table].forEach((elem, idx) => {
    if (elem.name === toFiler(props.field) && !props.plus && !elem.plus) {
      state[table].splice(idx, 1);
    }
  });
  let obj = {
    name: toFiler(props.field),
    condition: props.select,
    value: props.condition,
    plus: props.plus
  };
  state[table].push(obj);
}

const delFilter = (state, props) => {
  let table = 'mainFilterConditions';
  if (state.filteredTable === 2) {
    table = 'subFilterConditions'
  }
  state[table].forEach((elem, idx) => {
    if (!props.plus) {
      if (elem.name === toFiler(props.field) && !elem.plus) {
        state[table].splice(idx, 1);
      }
    } else {
      if (elem.value === props.condition) {
        state[table].splice(idx, 1);
      }
    }
  });
  if (!props.plus) {
    state.filterElement.filterCondition = null;
    state.filterElement.filterValue = '';
  }

}

// изменение дерева Географии
const buildTree = (state, data) => {
  let treeName = data.treeName
  data = data.data.filter(el => el.attributes['territory-is-parent']);
  data = data.map(el => ({...el, id: el.attributes.nid, parentid: el.attributes['parent-nid']}))

  function unflatten(arr) {
    var tree = [],
      mappedArr = {},
      arrElem,
      mappedElem;

    // First map the nodes of the array to an object -> create a hash table.
    for (var i = 0, len = arr.length; i < len; i++) {
      arrElem = arr[i];
      mappedArr[arrElem.id] = arrElem;
      mappedArr[arrElem.id]['children'] = [];
    }


    for (var id in mappedArr) {
      if (mappedArr.hasOwnProperty(id)) {
        mappedElem = mappedArr[id];
        // If the element is not at the root level, add it to its parent array of children.
        if (mappedElem.parentid) {
          mappedArr[mappedElem['parentid']]['children'].push(mappedElem);
        }
        // If the element is at the root level, add it to first level elements array.
        else {
          tree.push(mappedElem);
        }
      }
    }
    return tree;
  }

  state[treeName] = unflatten(data)
}

//Построение дерева сотрудников
const buildSupEmployeesTree = (state, data) => {
  let employeesTree = [{
    attributes: {
      name: 'Показать все',
    },
    id: 'all-1',
    levelType: 'all',
    children: []
  }];
  let hashParentItems = ['Показать все',];
  let hashChildrenItems = [];
  data.data.forEach((elem) => {
    if (hashParentItems.includes(elem.attributes['user-business-unit'])) {
      if (!hashChildrenItems.includes(elem.attributes['user-manager'])) {
        hashChildrenItems.push(elem.attributes['user-manager'])
        let idx = hashParentItems.indexOf(elem.attributes['user-business-unit'])
        elem.id = elem.attributes['nid']
        elem.attributes.name = elem.attributes['user-manager'] || 'Не определено';
        employeesTree[idx].children.push(elem)
      }
    } else {
      hashParentItems.push(elem.attributes['user-business-unit'])
      elem.children = [];
      elem.id = elem.attributes['nid'];
      elem.attributes.name = elem.attributes['user-business-unit'] || 'Не определено'
      elem.type = 'parent'
      employeesTree.push(elem);
    }
  })
  state.subEmployeesTree = employeesTree
}

const buildMasterEmployeesTree = (state, data) => {
  const createNewElem = (elem, param1, param2, param3) => {
    return {
      attributes: {
        ...elem.attributes,
        name: elem.attributes[param1] || 'Не определено',
      },
      id: elem.attributes['nid'],
      children: [
        {
          attributes: {
            ...elem.attributes,
            name: elem.attributes[param2] || 'Не определено'
          },
          children: [],
          levelType: param3
        }
      ],
    };
  };
  let masterEmployeesTree = [{
    attributes: {
      name: 'Показать все',
    },
    id: 'all-1',
    levelType: 'all',
    children: []
  }];
  let hashFirstLevelItems = ['Показать все',];
  let hashSecondLevelItems = [];
  let hashThirdLevelItems = [];
  data.data.forEach((elem) => {
    if (hashFirstLevelItems.includes(elem.attributes['employee-b-u'])) {
      if (!hashSecondLevelItems.includes(elem.attributes['employee-franchise'])) {
        let newElem = createNewElem(elem, 'employee-franchise', 'employee-sales-team', 'employee-franchise')
        newElem.levelType = 'employee-franchise'

        let idx = hashFirstLevelItems.indexOf(elem.attributes['employee-b-u'])
        masterEmployeesTree[idx].children.push(newElem)
        hashSecondLevelItems.push(elem.attributes['employee-franchise'])
      } else {
        if (!hashThirdLevelItems.includes(elem.attributes['employee-sales-team'])) {
          let idx1 = null;
          let idx2 = null;
          masterEmployeesTree.forEach((firstItem, index1) => {
            firstItem.children.forEach((secondItem, index2) => {
              if (secondItem.attributes['employee-franchise'] === elem.attributes['employee-franchise']) {
                idx2 = index2;
                idx1 = index1
              }
            })
          })
          let newElem = {
            attributes: {
              ...elem.attributes,
              name: elem.attributes['employee-sales-team'] || 'Не определено',
            },
            id: elem.attributes['nid'],
          };
          newElem.levelType = 'employee-sales-team'
          masterEmployeesTree[idx1].children[idx2].children.push(newElem)
          hashThirdLevelItems.push(elem.attributes['employee-sales-team'])
        }
      }
    } else {
      hashFirstLevelItems.push(elem.attributes['employee-b-u'])
      hashSecondLevelItems.push(elem.attributes['employee-franchise'])
      let newElem = createNewElem(elem, 'employee-b-u', 'employee-franchise', 'employee-b-u')
      newElem.levelType = 'employee-b-u'
      masterEmployeesTree.push(newElem)
    }

  });
  state.masterEmployeesTree = masterEmployeesTree
}

// апдейт после мэппинга
const updSubStr = (state, props) => {
  state.subTableData.splice(props.idx, 1, props.item)
}

// Смена текущий страницы данных
const updatePageNumber = (state, props) => {
  state.page[props.table].currentPage = props.pageNum
}

export default {
  updateTablesList,
  updateData,
  updateSubData,
  updateTablesCols,
  sortArray,
  updBrandList,
  updFilterConditions,
  delFilter,
  buildTree,
  buildSupEmployeesTree,
  buildMasterEmployeesTree,
  updSubStr,
  updatePageNumber
}