// получение списка таблиц
const fetchTablesList = ({commit}) => {
  fetch('http://176.118.31.82:8080/category', {
    method: 'get'
  }).then((res) => {
    return res.json()
  }).then((data) => {
    commit('updateTablesList', data.data);
  })
}

// Получение списка колонок
const fetchTablesCols = ({commit}) => {
  fetch('http://176.118.31.82:8080/displayed-fields', {
    method: 'get'
  }).then((res) => {
    return res.json()
  }).then((data) => {
    commit('updateTablesCols', data.data);
  })
}

// пролучение списка мастера
const fetchMasterTable = ({commit, state}) => {
  let url = `http://176.118.31.82:8080/${state.masterBackendName}`
  // TODO: Костыль 2. Здесь тоже хотелось бы увидеть паджинацию, но из-за необходимости построения дерева по всем данным, приходится исключать эту возможность из этого раздела
  if (state.activeMainTable.category !== 'Geography') url += `?page[number]=${state.page.master.currentPage}&page[size]=${state.page.size}`
  fetch(url, {
    method: 'get'
  }).then((res) => {
    return res.json()
  })
    .then((data) => {
      let obj = {
        data: data.data,
        flag: false,
        treeName: 'masterTerritoryTree',
        totalRecords: data.meta['total-records']
      };
      commit('updateData', obj);
      if (state.activeMainTable.category !== 'Geography') {
        commit('updBrandList', obj);
      }
      if (state.activeMainTable.category === 'Geography') {
        commit('buildTree', obj)
      }
      if (state.activeMainTable.category === 'Employees') {
        commit('buildMasterEmployeesTree', obj)
      }
    })
}

// пролучение списка sub
const fetchSubTable = ({commit, state, dispatch}) => {
  fetch(`http://176.118.31.82:8080/${state.subBackendName}?page[number]=${state.page.sub.currentPage}&page[size]=${state.page.size}`, {
    method: 'get'
  }).then((res) => {
    return res.json()
  })
    .then((data) => {
      let obj = {
        data: data.data,
        flag: false,
        totalRecords: data.meta['total-records']
      };
      commit('updateSubData', obj);
      if (state.activeSubTable.attributes.category === 'Geography') {
        dispatch('fetchSubTableTree')
      } else if (state.activeSubTable.attributes.category === 'Employees') {
        // dispatch('fetchSubTableEmployeesTree')
        let obj = {
          data: data.data,
          treeName: 'subTableEmployeesTree'
        };
        commit('buildSupEmployeesTree', obj)
      }
    })
}

const fetchSubTableTree = ({commit, state}) => {
  fetch(`http://176.118.31.82:8080/${state.subBackendName}?filter[territory-is-parent]=eq:true`, {
    method: 'get'
  }).then((res) => {
    return res.json()
  })
    .then((data) => {
      let obj = {
        data: data.data,
        treeName: 'subTerritoryTree'
      };
      commit('buildTree', obj)
    })
}

const fetchSubTableEmployeesTree = ({commit, state}) => {
  fetch(`http://176.118.31.82:8080/${state.subBackendName}`, {
    method: 'get'
  }).then((res) => {
    return res.json()
  })
    .then((data) => {
      let obj = {
        data: data.data,
        treeName: 'subTableEmployeesTree'
      };
      commit('buildSupEmployeesTree', obj)
    })
}


// фильтрация master таблицы
const filterMasterTable = ({commit, state}, closePopup) => {
  let filterPath = `http://176.118.31.82:8080/${state.masterBackendName}?`;
  state.mainFilterConditions.forEach((elem) => {
    let plusPath = `filter[${elem.name}]=${elem.condition}:${elem.value}&`;
    filterPath += plusPath;
  });

  fetch(filterPath, {
    method: 'get'
  }).then((res) => {
    return res.json()
  }).then((data) => {
    let obj = {
      data: data.data,
      flag: true
    };
    commit('updateData', obj);
    closePopup ? state.filterIsActive = false : null
  })
}

// фильтрация sub таблицы
const filterSubTable = ({commit, state}, closePopup) => {
  let filterPath = `http://176.118.31.82:8080/${state.subBackendName}?page[number]=1&page[size]=100&`;
  state.subFilterConditions.forEach((elem) => {
    let plusPath = `filter[${elem.name}]=${elem.condition}:${elem.value}&`;
    filterPath += plusPath;
  });

  fetch(filterPath, {
    method: 'get'
  }).then((res) => {
    return res.json()
  }).then((data) => {
    let obj = {
      data: data.data,
      flag: true
    };
    commit('updateSubData', obj);

    closePopup ? state.filterIsActive = false : null
  })
}

// фильтрация таблицы через инпут
const filterByInput = ({commit, state}, props) => {
  let filterPath = `http://176.118.31.82:8080/${state.masterBackendName}?filter[${props.name}]=like:${props.value}&`;
  if (props.id === 2) {
    filterPath = `http://176.118.31.82:8080/${state.subBackendName}?page[number]=1&page[size]=100&filter[${props.name}]=like:${props.value}&`;
  }

  fetch(filterPath, {
    method: 'get'
  }).then((res) => {
    return res.json()
  }).then((data) => {
    let obj = {
      data: data.data,
      flag: true
    };
    if (props.id === 1) {
      commit('updateData', obj);
    } else {
      commit('updateSubData', obj);
    }
    state.activeFilter = false
  })
}

export default {
  fetchTablesList,
  fetchTablesCols,
  fetchMasterTable,
  fetchSubTable,
  fetchSubTableTree,
  fetchSubTableEmployeesTree,
  filterMasterTable,
  filterSubTable,
  filterByInput
}