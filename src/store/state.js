const state = {
  masterTables: [],
  subTablesList: [],
  masterBackendName: '',
  subBackendName: '',
  treeData: [],
  mainTableData: [],
  mainTableCols: [],
  subTableData: [],
  subTableCols: [],
  activeTab: null,
  activeTabName: '',
  activeMainTableName: '',
  edited: null,
  activeFilter: false,
  filterIsActive: false,
  filteredField: 'Наименование ',
  activeMainTable: null,
  activeSubTable: null,
  activeMainFields: [],
  activeSubFields: [],
  filteredTable: null,
  mainFilterConditions: [],
  subFilterConditions: [],
  brandList: [],
  dragElem: null,
  onlySub: false,
  isAuthenticated: false,
  // дерево для географии
  masterTerritoryTree: null,
  subTerritoryTree: null,
  // дерево для сотрудников
  subEmployeesTree: null,
  masterEmployeesTree: null,
  // Используется для создания паджинации
  page: {
    size: 100,
    master: {
      currentPage: 1,
      pagesCount: 0
    },
    sub: {
      currentPage: 1,
      pagesCount: 0
    }
  }
}

export default state