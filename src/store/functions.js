export const sortCols = (data, cols) => {
  let colsKeys = Object.keys(data[0])
  cols.sort((a, b)=>{
    let p1 = colsKeys.indexOf(toKebabCase(a.dataName))
    let p2 = colsKeys.indexOf(toKebabCase(b.dataName))
    if (p1 > p2) {
      return 1;
    }
    if (p1 < p2) {
      return -1;
    }
    return 0;
  })
}

export const toKebabCase = (str) => {
  return str.replace(/ /g, '').toLowerCase()
}

export const toFiler = (str) => {
  return  str.replace(/ /g, '').replace(/([A-Z]+)/g, '-$1').replace(/^-/, '').toLowerCase()
}

export const fromKebabCase = (str) => {
  return str.replace(/-/g, ' ').replace(/( |^)[a-z]/g, function(x){ return x.toUpperCase(); } )
}

export const getFilterCategories = (state, dataName, table) => {
  let FilterCategories = [];
  state[table].forEach((elem)=>{
    let name = '';
    let flag = true;
    FilterCategories.forEach((cat)=>{
      if(cat.name===elem[toKebabCase(dataName)] && flag){
        flag = false;
      }
    });
    if(flag){
      name = elem[toKebabCase(dataName)];
      if(name){
        let obj = {
          name: name,
          isActive: true
        };
        FilterCategories.push(obj)
      }
    }
  });
  return FilterCategories
}