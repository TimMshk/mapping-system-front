import Vue from 'vue'
import Router from 'vue-router'
import Page404 from '../views/page404'
import Home from '../views/home'
import mapping from '../views/mapping'
import test from "../views/test";
import Login from "../views/Login";
import store from "@/store/index"

let VueCookie = require('vue-cookie');
Vue.use(Router)

let router =  new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/mapping',
      name: 'mapping',
      component: mapping,
      beforeEnter(to, from, next){
        if(!store.state.activeMainTable){
          next('/')
        } else{
          next()
        }
      }
    },
    {
      path: '/test',
      name: 'test',
      component: test
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '*',
      name: '404',
      component: Page404
    }
  ],
  mode: 'history',
});

router.beforeEach((to, from, next)=>{
  let isAuthenticated = VueCookie.get('isAuthenticated');
  if(!isAuthenticated && to.path !== '/login'){
    next('/login')
  } else{
    next()
  }
});

export default router
