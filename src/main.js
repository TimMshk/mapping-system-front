import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Paginate from 'vuejs-paginate'

import vuetify from './plugins/vuetify'

let VueCookie = require('vue-cookie');
Vue.component('paginate', Paginate)

import Vuex from 'vuex';
Vue.use(Vuex);
Vue.use(VueCookie);

Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
